import Vue from 'vue'

import pseudoApiMixin from '@/mixins/pseudoApiMixin'
Vue.mixin(pseudoApiMixin);

import VueYoutube from 'vue-youtube'

Vue.use(VueYoutube)

let requireComponent = require.context("@/components/patterns", true, /.*\.vue$/);
//console.log('requireComponent.keys()',requireComponent.keys());
requireComponent.keys().forEach(function(fileName) {
    let baseComponentConfig = requireComponent(fileName);
    baseComponentConfig = baseComponentConfig.default || baseComponentConfig;
    let baseComponentName =
        baseComponentConfig.name ||
        fileName.replace(/^.+\//, "").replace(/\.\w+$/, "");
    //console.log(baseComponentName);
    Vue.component(baseComponentName, baseComponentConfig);
   
});
requireComponent = require.context("@/components/blocks", true, /.*\.vue$/);
//console.log('requireComponent.keys()',requireComponent.keys());
requireComponent.keys().forEach(function(fileName) {
    let baseComponentConfig = requireComponent(fileName);
    baseComponentConfig = baseComponentConfig.default || baseComponentConfig;
    let baseComponentName =
        baseComponentConfig.name ||
        fileName.replace(/^.+\//, "").replace(/\.\w+$/, "");
    //console.log(baseComponentName);
    Vue.component(baseComponentName, baseComponentConfig);
   
});
requireComponent = require.context("@/components/global", true, /.*\.vue$/);
//console.log('requireComponent.keys()',requireComponent.keys());
requireComponent.keys().forEach(function(fileName) {
    let baseComponentConfig = requireComponent(fileName);
    baseComponentConfig = baseComponentConfig.default || baseComponentConfig;
    let baseComponentName =
        baseComponentConfig.name ||
        fileName.replace(/^.+\//, "").replace(/\.\w+$/, "");
    //console.log(baseComponentName);
    Vue.component(baseComponentName, baseComponentConfig);
   
});
