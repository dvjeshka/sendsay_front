export const getBaseUrl = () => {
  return typeof (window) !== 'undefined' && !process.server
    ? `${window.location.protocol}//${window.location.hostname}` : 'https://sendsay.ru'
}
