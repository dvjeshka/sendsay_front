export default function ruble (bold) {
  let sign = '*'
  if (typeof (bold) !== 'undefined' && bold) {
    sign = ')'
  }
  //return `<span class="ruble">${sign}</span>`
  return `<span class="ruble">₽</span>`
}
