/* eslint-disable */
const emailRe = /^(.+)@(.+)\.(.+)$/;
/* eslint-enable */

export const email = value => emailRe.test(value)
