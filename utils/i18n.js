import config from '../config'

export const LOCALE_STORAGE_KEY = 'locale'

export function testLocale (locale) {
  return locale && config.locales.indexOf(locale) >= 0
}

export function getLocale (req) {
  let locale = null

  if (process.browser && typeof (window) !== 'undefined') {
    let urlParts = window.location.pathname.split('/')
    let urlLocale = urlParts.length > 1 ? urlParts[1] : ''
    if (config.locales.indexOf(urlLocale) >= 0) {
      locale = urlLocale
    }
  }

  if (!locale && process.browser) {
    try {
      if (typeof (localStorage) !== 'undefined' && !!localStorage.getItem) {
        locale = localStorage.getItem(LOCALE_STORAGE_KEY) || null
      }
    } catch (e) {}
  }

  if (!testLocale(locale)) {
    if (req && req.headers && req.headers['accept-language']) {
      locale = req.headers['accept-language'].split(',')[0].toLocaleLowerCase().substring(0, 2)
    }
  }

  if (!testLocale(locale)) {
    if (typeof (navigator) !== 'undefined') {
      locale = navigator.language.toLocaleLowerCase().substring(0, 2)
    }
  }

  if (!testLocale(locale)) {
    locale = config.locales[0]
  }

  return locale
}

export function saveLocale (locale) {
  if (config.locales.indexOf(locale) >= 0) {
    if (typeof (localStorage) !== 'undefined' && !!localStorage.setItem) {
      localStorage.setItem(LOCALE_STORAGE_KEY, locale)
    }
  }
}
