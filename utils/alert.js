import eventHub from '../utils/eventHub'

export const Alert = (title, content) => {
  eventHub.$emit('global:alert', { title, content })
}

export const Confirm = (title, content, callback) => {
  eventHub.$emit('global:confirm', { title, content, callback })
}

export const errorResponseAlert = (response, msg) => {
  if (!msg) {
    msg = ''
  }

  console.log(`${msg}:`)
  console.log(response.data)
  let data = response.data

  if (typeof (data.detail) !== 'undefined' && data.detail) {
    let detail = data.detail

    if ('errors' in detail) {
      let errors = detail.errors
      detail = []
      errors.forEach(value => {
        let message = value.message
        if (value.name === '__all__') {
          message = value.message
        }
        detail.push(message)
      })
      detail = detail.join('<br>')
    }

    eventHub.$emit('global:alert', {
      title: detail
    })
  } else {
    eventHub.$emit('global:alert', {
      content: response.data,
      title: ''
    })
  }
}
