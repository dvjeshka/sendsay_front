export async function fetchPage (app, error, url) {
  let result = {
    fetchError: false,
    fetchStatus: 200,
    page: {}
  }

  try {
    const response = await app.$axios.get(url)
    result.fetchStatus = response && response.status ? response.status : 0
    result = Object.assign({}, result, response.data || {})
  } catch ({ response }) {
    result.fetchError = true
    result.fetchStatus = response && response.status ? response.status : 0

    if (result.fetchStatus >= 400) {
      error({
        statusCode: response.status,
        message: response.data.detail || ''
      })
    }
  }

  return result
}
