export const METRIKA_ID = '5230714'

export function getMetrikaCounter () {
  if (process.browser && typeof (window['yaCounter' + METRIKA_ID]) !== 'undefined') {
    return window['yaCounter' + METRIKA_ID]
  }
  return null
}

export function execMetrikaMethod (method, args, attempts) {
  attempts = attempts || 0
  let counter = getMetrikaCounter()
  if (counter) {
    counter[method].apply(counter, args)
  } else {
    if (process.browser && attempts < 1) {
      document.addEventListener('yacounter' + METRIKA_ID + 'inited', () => {
        execMetrikaMethod(method, args, attempts + 1)
      })
    }
  }
}
