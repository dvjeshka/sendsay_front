export function getCookie (name) {
  if (process.server) {
    return null
  }

  let matches = document.cookie.match(new RegExp(
    '(?:^|; )' + name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1') + '=([^;]*)'
  ))
  return matches ? decodeURIComponent(matches[1]) : undefined
}

export function setCookie (name, value, options) {
  if (process.server) {
    return false
  }

  options = options || {}

  let expires = options.expires

  if (typeof expires === 'number' && expires) {
    let d = new Date()
    d.setTime(d.getTime() + expires * 1000)
    expires = options.expires = d
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString()
  }

  value = encodeURIComponent(value)

  let updatedCookie = name + '=' + value

  for (var propName in options) {
    if (options.hasOwnProperty(propName)) {
      updatedCookie += '; ' + propName
      var propValue = options[propName]
      if (propValue !== true) {
        updatedCookie += '=' + propValue
      }
    }
  }

  document.cookie = updatedCookie
  return true
}

export function removeCookie (name) {
  setCookie(name, '', { expires: -999999 })
}
