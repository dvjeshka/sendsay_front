import { getBaseUrl } from './baseUrl'

export const pageMeta = (vm, cards) => {
  cards = typeof (cards) === 'undefined' ? true : cards

  let canonical = `${getBaseUrl()}${vm.$route.path}`
  let meta = {
    title: vm.prepareTitle(vm.metaTitle),
    meta: [
      { hid: 'description', name: 'description', content: vm.metaDescription },
      { hid: 'keywords', name: 'keywords', content: vm.metaKeywords }
    ],
    link: [{ rel: 'canonical', href: canonical }]
  }

  if (cards) {
    meta.meta.push(
      { hid: 'og:title', name: 'og:title', content: vm.metaTitle },
      { hid: 'og:description', name: 'og:description', content: vm.metaDescription },
      { hid: 'twitter:title', name: 'twitter:title', content: vm.metaTitle },
      { hid: 'twitter:description', name: 'twitter:description', content: vm.metaDescription }
    )
  }

  return meta
}
