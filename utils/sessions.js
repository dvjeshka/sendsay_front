import { getCookie, setCookie } from './cookies'

export const COOKIE_SESSION_NAME = 'sendsay_session'
export const COOKIE_LOGIN_NAME = 'sendsay_login'
export const COOKIE_SUBLOGIN_NAME = 'sendsay_sublogin'

export function getSession () {
  let session = null
  if (process.browser) {
    return getCookie(COOKIE_SESSION_NAME) || null
  }

  return session
}

export function setSession (sessID) {
  if (process.browser) {
    setCookie(COOKIE_SESSION_NAME, sessID, {
      expires: (24 * 60 * 60) - 60,
      domain: '.sendsay.ru',
      'path': '/'
    })
    return true
  }
  return false
}
