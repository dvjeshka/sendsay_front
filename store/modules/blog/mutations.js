export const SET_BLOG_POSTS = (state, payload) => {
  state.posts = (payload || [])
}

export const SET_BLOG_POSTS_FAIL = (state) => {
  state.posts = null
}

export default {
  SET_BLOG_POSTS,
  SET_BLOG_POSTS_FAIL
}
