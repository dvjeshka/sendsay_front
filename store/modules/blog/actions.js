import API from '../../../api'

function getBlogPosts ({ state, commit }) {
  return new Promise((resolve, reject) => {
    if (state.posts !== null) {
      resolve(state)
      return
    }

    this.$axios
      .get(API.blog.posts(), { params: { limit: 3 } })
      .then(response => {
        commit('SET_BLOG_POSTS', response.data.results)
        resolve(response)
      })
      .catch(({ response }) => {
        commit('SET_BLOG_POSTS_FAIL', null)
        reject(response)
      })
  })
}

export default { getBlogPosts }
