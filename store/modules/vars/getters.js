export default {
  menus: (state) => { return state.menus },
  settings: (state) => { return state.settings }
}
