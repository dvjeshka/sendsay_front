export const SET_SETTINGS = (state, payload) => {
  state.settings = (payload || {}).site_config || {}
  const menus = (payload || {}).menus || []
  let menuObj = {}
  menus.forEach(m => {
    menuObj[m.slug] = m
  })
  state.menus = menuObj
}

export const SET_SETTINGS_FAIL = (state) => {
  state.settings = null
  state.menus = null
}

export default {
  SET_SETTINGS,
  SET_SETTINGS_FAIL
}
