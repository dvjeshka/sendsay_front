import blog from './blog'
import services from './services'
import vars from './vars'

export default {
  blog,
  services,
  vars
}
