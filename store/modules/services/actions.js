import API from '../../../api'

function allServices ({ state, commit }) {
  return new Promise((resolve, reject) => {
    if (state.services !== null) {
      resolve(state)
      return
    }

    Promise.all([
      this.$axios.get(API.services.opportunities()),
      this.$axios.get(API.services.services()),
      this.$axios.get(API.services.solutions())
    ]).then(values => {
      commit('SET_ALL_SERVICES', {
        opportunities: values[0].data.results,
        services: values[1].data.results,
        solutions: values[2].data.results
      })
      resolve(values)
    }, ({ response }) => {
      commit('SET_SERVICES_FAIL', null)
      reject(response)
    })
  })
}

export default { allServices }
