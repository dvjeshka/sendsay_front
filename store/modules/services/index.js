import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export default {
  actions,
  getters,
  mutations,
  namespaced: true,
  state: () => ({
    opportunities: null,
    services: null,
    solutions: null
  })
}
