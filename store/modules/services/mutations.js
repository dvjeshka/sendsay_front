export const SET_ALL_SERVICES = (state, payload) => {
  state.opportunities = (payload || {}).opportunities || {}
  state.services = (payload || {}).services || {}
  state.solutions = (payload || {}).solutions || {}
}

export const SET_SERVICES_FAIL = (state) => {
  state.opportunities = null
  state.services = null
  state.solutions = null
}

export default {
  SET_ALL_SERVICES,
  SET_SERVICES_FAIL
}
