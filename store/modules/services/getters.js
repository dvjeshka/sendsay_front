export default {
  opportunities: (state) => { return state.opportunities },
  services: (state) => { return state.services },
  solutions: (state) => { return state.solutions }
}
