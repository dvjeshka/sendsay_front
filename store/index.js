import Vuex from 'vuex'
import getters from './getters'
import modules from './modules'
import mutations from './mutations'
import { getSession } from '../utils/sessions'

const store = () => new Vuex.Store({
  getters,
  modules,
  mutations,
  state: () => ({
    mobileExpanded: false,
    session: getSession(),
  window:{
      height:null,
      width:null
  },
  })
})

export default store
