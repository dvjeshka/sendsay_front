export default {
  isAuthenticated: state => !!state.session
}
