import { getSession, setSession } from '../utils/sessions'

export const HIDE_MENU = (state) => {
  state.mobileExpanded = false
}

export const GET_SESSION_FROM_COOKIE = (state) => {
  state.session = getSession()
}

export const SET_SESSION = (state, session) => {
  state.session = session || null
  setSession(session)
}

export const TOGGLE_MENU = (state) => {
  state.mobileExpanded = !state.mobileExpanded
}

/* export const UNSET_SESSION = (state) => {
  state.session = null
  removeCookie(COOKIE_SESSION_NAME)
  removeCookie(COOKIE_LOGIN_NAME)
  removeCookie(COOKIE_SUBLOGIN_NAME)
} */

export const setWindowSize = (state, {width,height}) => {
    state.window.width = width
    state.window.height = height
}

export default {
  GET_SESSION_FROM_COOKIE,
  HIDE_MENU,
  SET_SESSION,
  TOGGLE_MENU,
  setWindowSize
}




