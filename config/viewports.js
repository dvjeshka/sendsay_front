module.exports = {
    'viewport-desktop-max':'1920px',
    'viewport-desktop-medium':'1600px',
    'viewport-desktop-min':'1200px',
    'viewport-tablet-max':'1024px',
    'viewport-tablet-medium':'992px',
    'viewport-tablet-min':'768px',
    'viewport-phone-max':'650px',
    'viewport-phone-medium':'480px',
    'viewport-phone-min':'320px',
}
