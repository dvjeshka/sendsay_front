


export default {
    title: 'VBtn',
   
}

/*export const VBtn = ({small,color}) => `
<div>
<VBtn small> привет</VBtn>
<VBtn color="primary"> Как дела</VBtn>
</div>

`*/
import { action } from '@storybook/addon-actions'

export const MyAwesomeButton = ({small,color}) => ({
    template: `<v-btn @click.native="clickme" :label="label" :color="color" />`,
    props: {
        label: {
            default: 'Awesome button !!!!'
        },
        color: {
            default: '#777777'
        }
    },
    methods: {
        clickme () {
            action('button-click')()
        }
    },
  
})

MyAwesomeButton.argTypes = {
    label: {
        type: { name: 'string' },
        defaultValue: "Awesome button qweqweqweqweqwe"
    },
    color: { control: 'color' },
}




export const VBtnTest = ({str}) => ({
    template: `<VBtnTest :textV="str"></VBtnTest>`,
    props: {
        str: {
            default: 'Awesome button !!!!'
        },
      
    },
  

})

VBtnTest.args = {
    str: 'hello'
}

/*export const Test = VBtnTest.bind({});
Test.args = {
    msg: 'hello'
}

export const TestTwo = (args) => ({
    template: `
        <div>
            <div>
                {{text}}
            </div>
        </div>
    `,
    props: {
        text: {
            type: String,
        }
    },
});

TestTwo.args = {
    text: 'asdf'
}*/

// We create a “template” of how args map to rendering


