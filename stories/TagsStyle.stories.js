


export default {
    title: 'TagsStyle',
   
}

export const TagsStyleAll = () => '<TagsStyleExample />'
export const VListUl = () => `
    <VList>
        <template slot="item">1</template>
        <template slot="item">2</template>
        <template slot="item">3</template>
        <template slot="item">4</template>
        <template slot="item">5</template>
    </VList>`

export const VListOl = () => `
    <VList type="ol">
          <template slot="item">1</template>
        <template slot="item">2</template>
        <template slot="item">3</template>
        <template slot="item">4</template>
        <template slot="item">5</template>
    </VList>
    `

