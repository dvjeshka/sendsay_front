import { action } from '@storybook/addon-actions'

export default {
    title: 'GUI'
}

export const Страница = () => `<GuiExample></GuiExample>`;
export const AppSelect = () => `<AppSelect></AppSelect>`;
export const Hexagon = (color) => `<HexFigure>`;
export const Списки = ({tag}) => ({
    template: `
        <v-list :tag="tag">
            <template slot="item">Определение лучшего времени доставки сообщения каждому клиенту</template>
            <template slot="item">Персональные товарные рекомендации</template>
            <template slot="item">Динамический контент</template>
            <template slot="item">Триггерные и транзакционные письма по любым сценариям.</template>
        </v-list>`,
    props: {
        props: {
            tag: { type:String, default:'ul' }
        },
    },
    methods: {
        clickme () {
            action('button-click')()
        }
    },
})

Списки.args = {
    tag: { type:String, default:'ol' }
}




Hexagon.props = {
    color: {
        type: String,
        default: 'red',
    }
};
