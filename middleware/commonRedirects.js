export default function ({ app, error, params, redirect, req, route }) {
  let url = route.fullPath
  let status = 200
  let indexFilesRegEx = /index\.(html|htm|php)$/
  let endingRegEx = /\.(html|htm|php)$/

  // index files to /
  if (indexFilesRegEx.test(url)) {
    url = url.replace(indexFilesRegEx, '')
    status = Math.max(status, 301)
  }

  // extensions /
  if (endingRegEx.test(url)) {
    url = url.replace(endingRegEx, '')
    status = Math.max(status, 301)
  }

  // remove trailing slash
  if (url.length > 1 && /\/$/.test(url)) {
    url = url.substr(0, url.length - 1)
    status = Math.max(status, 301)
  }

  // i18n
  /* if (params.locale && params.locale.length === 2) {
    if (config.locales.indexOf(params.locale) >= 0) {
      changeLocale(params.locale, app, store)
    } else {
      error({ statusCode: 404, message: 'LocaleError' })
    }
  } else {
    // проверим, указана ли языковая переменная
    let urlParts = url.split('/')
    let locale = urlParts.length > 1 ? urlParts[1] : ''

    if (config.locales.indexOf(locale) >= 0) {
      changeLocale(locale, app, store)
    } else {
      // языковая переменная неправильная, либо не указана
      if (locale.length === 2) {
        error({ statusCode: 404, message: 'LocaleError' })
      } else {
        changeLocale(getLocale(req), app, store)
        url = url === '/' ? '' : url
        status = url === '' ? 302 : 301
        url = `/${store.state.locale}${url}`
      }
    }
  } */

  if (status > 300) {
    return redirect(status, url)
  }
}
