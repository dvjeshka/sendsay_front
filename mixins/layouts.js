const fadeIn = (vm) => {
  let loader = document.getElementById('loader')
  if (loader) {
    loader.classList.add('hide')

    setTimeout(() => {
      if (loader.parentNode) {
        loader.parentNode.removeChild(loader)
      }
    }, 350)
  }
}

export const layoutMixin = {
  mounted () {
    let vm = this
    if (process.browser) {
      let timer = setTimeout(() => fadeIn(vm), 700)
      window.onload = () => {
        if (timer) {
          clearTimeout(timer)
        }
        fadeIn(vm)
      }
    }
  }
}
