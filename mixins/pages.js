import { fetchPage } from '../utils/asyncPages'
import { pageMeta } from '../utils/pageMeta'


export const pageMixin = {
  async created () {
      console.log('console');
      this.parseResponse()
  },

  data () {
    return {
      metaDescription: '',
      metaKeywords: '',
      metaTitle: ''
    }
  },

  async fetch () {
    try {
      await Promise.all([
        this.$store.dispatch('blog/getBlogPosts'),
        this.$store.dispatch('services/allServices'),
        this.$store.dispatch('vars/settings')
      ])
    } catch (e) {}
  },

  head () {
    return pageMeta(this)
  },

  methods: {
      GET_SESSION_FROM_COOKIE(){
        this.$store.commit('GET_SESSION_FROM_COOKIE')    
      },
   
    // если требуется перегрузка данных страницы на открытой странице
    async fetchPage (url) {
      let vm = this

      return new Promise(async (resolve, reject) => {
        try {
          const result = await fetchPage(this, () => {
            // eslint-disable-next-line prefer-promise-reject-errors
            reject(vm)
            return vm.$router.push({ name: 'errors-404' })
          }, url)

          if (result.fetchError) {
            // eslint-disable-next-line prefer-promise-reject-errors
            reject(vm)
            return vm.$router.push({ name: 'errors-404' })
          }

          for (let key in result) {
            if (result.hasOwnProperty(key)) {
              vm[key] = result[key]
            }
          }

          vm.parseResponse()
          resolve(vm)
        } catch (error) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject(vm)
          vm.$router.push({ name: 'errors-404' })
        }
      })
    },

    onError () {},
    onSuccess () {},

    parseResponse () {
      if (this.fetchError) {
        this.onError()
        return false
      }

      this.$nextTick(() => {
        if (this.page && this.page.title) {
          this.metaTitle = this.page.title
        } else if (typeof (this.title) !== 'undefined' && this.title) {
          this.metaTitle = this.title
        }

        let meta = this.meta || null
        if (this.page && this.page.meta) {
          meta = this.page.meta
        }

        if (meta && meta.seo_description) {
          this.metaDescription = meta.seo_description
        }
        if (meta && meta.seo_keywords) {
          this.metaKeywords = meta.seo_keywords
        }
        if (meta && meta.seo_title) {
          this.metaTitle = meta.seo_title
        }

        this.onSuccess()
      })
    },

    prepareTitle (title) {
      if (this.vars && (this.vars.SEO_TITLE_PREFIX || this.vars.SEO_TITLE_POSTFIX)) {
        let prefix = this.vars.SEO_TITLE_PREFIX || ''
        let postfix = this.vars.SEO_TITLE_POSTFIX || ''
        return `${prefix} ${title} ${postfix}`
      } else {
        return title
      }
    }
  },

  async mounted () {
    // фикс отсутствия скролла наверх на некоторых страницах
    if (process.browser) {
      window.scrollTo(0, 0)
      this.GET_SESSION_FROM_COOKIE()
    }
  }
}
