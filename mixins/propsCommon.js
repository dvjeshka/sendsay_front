export default {
    props:{
        title:{
            type:String,
            default:''
        },
        content:{
            type:String,
            default:''
        }
    }
}
