export default {
    computed:{
        contentPseudoApi(){
            return {
                title:'Просто текстовый блок',
                content:'<ul><li>Хранение и обработка контактов соответствущее тарифу</li><li>Неограниченное количество писем в месяц</li><li>Неограниченное push рассылок и сообщений ВКонтакте</li><li>Профессиональные инструменты сегментации и анализа</li><li>Визуальный редактор сообщений, писем и форм подписки</li><li>Полная автоматизация: триггеры, товарные рекомендации, индивидуальное время доставки</li></ul>',

            }
        },
        subscribeFormPseudoApi(){
            return {
                title:'Получайте бесплатные советы',
                content:'<p>Мы не будем закидывать Вас пустыми письмами, только полезные советы, мнения и кейсы</p>',
                image:'/upload/img/content-banner-01.png',
                button_text:'Попробовать бесплатно',
                button_url:'http://ya.ru',
                color:'#091c48',
                agree:'<p>Нажимая на кнопку, Вы соглашаетесь с условиями <a href="#">обработки персональных данных</a></p>'
            }
        },
        vProfitPseudoApi(){
            return {
                title:'Не просто email письма. Мы знаем как заставить письма приноситьприбыль',
                content:'<ol><li>подбор индивидуального контента</li><li>время</li><li>канал</li></ol>',
                image:'/upload/img/content-banner-01.png',
                button_text:'Попробовать бесплатно',
                button_url:'http://ya.ru',
                color:'#091c48'
            }
        },
        vNewsPseudoApi(){
            return [
                {
                    ...this.VMediaNewsPseudoApi,
                    type:'medianews',
                    content:''
                },
                {
                    ...this.VMediaVideoPseudoApi,
                    type:'mediavideo',
                    content:'',
                    button_url:'/',
                },
                {
                    ...this.VMediaNewsPseudoApi,
                    type:'medianews',
                    button_url:'',
                    content:''
                },
                {
                    ...this.VMediaVideoPseudoApi,
                    type:'mediavideo',
                    button_url:'/',
                },
            ]    
        },
      
        VMediaNewsPseudoApi() {
            return {
                title:'СМИ о нас',
                content:'<p>Тестовый контент</p>',
                button_url:'/',
                itemList:[
                    {
                        id:1,
                        title:'Как изменился email- маркетинг за последние 5 лет?',
                        image:'/upload/img/banner-05.png',
                        content:'<p>И почему он остается одним из главных инструментов повторных продаж?</p>',
                        button_url:'/',
                        logoUrl:'/upload/img/news-logo-01.jpg',
                        logoTitle:'news-logo-01',
                      
                    },
                    {
                        id:1,
                        title:'Как изменился email- маркетинг за последние 5 лет? Как изменился email- маркетинг за последние 5 лет?',
                        image:'/upload/img/banner-05.png',
                        content:'<p>И почему он остается одним из главных инструментов повторных продаж?</p><p>И почему он остается одним из главных инструментов повторных продаж?</p>',
                        button_url:'/',
                        logoUrl:'/upload/img/news-logo-01.jpg',
                        logoTitle:'news-logo-01',
                      
                    },
                    {
                        id:1,
                        title:'Как изменился email- маркетинг за последние 5 лет?',
                        image:'/upload/img/banner-05.png',
                        content:'<p>И почему он остается одним из главных инструментов повторных продаж?И почему он остается одним из главных инструментов повторных продаж?</p>',
                        button_url:'',
                        logoUrl:'/upload/img/news-logo-01.jpg',
                        logoTitle:'news-logo-01',
                      
                    },
                ]
            }
        },
        VMediaVideoPseudoApi() {
            return {
                title:'Наши выступления',
                content:'<p>Тестовый контент</p>',
                button_url:'',
                itemList:[
                    {
                        id:1,
                        title:'Глеб Кащеев Директор по развитию на Sinergy Digital форуме',
                        content:'Все самые эффективные тренды в автоматизации повторных продаж в одном ролике',
                        image:'/upload/img/video-preview-01.jpg',
                        videoId:'M7lc1UVf-VE'
                    },
                    {
                        id:1,
                        title:'Глеб Кащеев Директор по развитию',
                        content:'Все самые эффективные тренды в автоматизации повторных продаж в одном ролике Все самые эффективные тренды в автоматизации повторных продаж в одном ролике',
                        image:'/upload/img/banner-05.png',
                        videoId:'M7lc1UVf-VE'
                    },
                ]
            }
        },
        socialsWrapperPseudoApi() {
            return {
                title:'Подписывайте на наши группы, будьте в курсе последних событий',
                itemList:[
                    {
                        id:1,
                        title:'fb',
                        url:'/upload/img/socials/fb.svg',
                        href:'/'
                    },
                    {
                        id:2,
                        title:'vk.svg',
                        url:'/upload/img/socials/vk.svg',
                        href:'/'
                    },
                    {
                        id:3,
                        title:'yt.svg',
                        url:'/upload/img/socials/yt.svg',
                        href:'/'
                    },



                ]
            }
        },
        equalPartsReversePseudoApi(){
            return {
                reverse:true,
                title:'Самые выгодные комбинации каналов',
                content:'<p>Email, Push, сообщения в соцсетях, мессенджерах и SMS c возможностью выбора оптимальных стратегий и сочетаний: как для расширения охвата, так и для минимизации бюджета</p>',
                image:'/upload/img/include-content-01.jpg',
                button_text:'href link',
                button_url:'http://ya.ru'
            }
        },
        equalPartsPseudoApi(){
            return {
                title:'Самые выгодные комбинации каналов',
                content:'<p>Email, Push, сообщения в соцсетях, мессенджерах и SMS c возможностью выбора оптимальных стратегий и сочетаний: как для расширения охвата, так и для минимизации бюджета</p>',
                image:'/upload/img/include-content-01.jpg',
                button_text:'href link',
                button_url:'http://ya.ru'
            }
        },
        
        vSafetyPseudoApi() {
            return {
                title:'Надежность и безопастность данных в Sendsay',
                content:'<p>Тестовый контент</p>',
                itemList:[
                    {
                        id:1,
                        title:'Автоматизация рассылок',
                        icon:'<svg width="125" height="152" viewBox="0 0 125 152" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M62.5 45.125a2.974 2.974 0 00-2.973 3.114l-.19.018.074-.003.116-.006a2.978 2.978 0 005.946-.006c.003-.047.003-.098.003-.148a2.974 2.974 0 00-2.976-2.969z" fill="#A02140"></path><path d="M124.009 18.08a2.983 2.983 0 00-2.313-.74 78.59 78.59 0 01-8.601.473c-18.711 0-37.493-8.903-48.83-17.234a2.982 2.982 0 00-3.53 0C49.398 8.91 30.616 17.813 11.905 17.813a78.75 78.75 0 01-8.601-.472 2.983 2.983 0 00-2.313.739A2.966 2.966 0 000 20.292v51.774c0 36.986 27.2 68.958 61.604 79.796a2.98 2.98 0 001.792 0C97.77 141.033 125 109.084 125 72.066V20.291c0-.844-.36-1.648-.991-2.211zm-4.961 53.986c0 32.702-23.205 62.939-56.548 73.847-33.343-10.908-56.548-41.145-56.548-73.847V23.54c1.98.14 3.972.21 5.953.21 19.202 0 38.364-8.625 50.595-17.13 12.23 8.505 31.393 17.13 50.595 17.13 1.981 0 3.973-.07 5.953-.21v48.526z" fill="#A02140"></path><path d="M110.24 29.628c-14.753-.596-31.59-6.034-46.19-14.917a2.983 2.983 0 00-3.1 0c-14.6 8.883-31.437 14.32-46.19 14.917a2.972 2.972 0 00-2.855 2.966v39.473c0 28.688 20.377 56.316 49.554 67.188a2.983 2.983 0 002.082 0c29.177-10.872 49.554-38.501 49.554-67.189V32.594a2.972 2.972 0 00-2.855-2.966zm-3.097 42.438c0 25.922-18.302 50.947-44.643 61.229-26.341-10.282-44.643-35.307-44.643-61.229V35.395c19.16-1.447 35.57-9.37 44.643-14.69 9.074 5.32 25.484 13.243 44.643 14.69v36.671z" fill="#A02140"></path><path d="M75.34 47.734a2.979 2.979 0 00-3.909 1.56 2.966 2.966 0 001.563 3.898c9.897 4.23 16.292 13.883 16.292 24.59 0 15.181-12.517 28.499-26.786 28.499-14.006 0-27.005-13.068-26.78-29.058.327-10.829 6.414-19.81 16.286-24.032a2.966 2.966 0 001.563-3.898 2.979 2.979 0 00-3.908-1.559C37.775 52.815 30.153 64.056 29.769 77.07v.047c-.265 18.302 14.319 35.102 32.731 35.102 18.328 0 32.738-16.657 32.738-34.438 0-13.085-7.81-24.879-19.899-30.047z" fill="#A02140"></path><path d="M79.485 66.776a2.982 2.982 0 00-4.209 0L56.548 85.458l-9.8-9.776a2.982 2.982 0 00-4.21 0 2.964 2.964 0 000 4.199l11.905 11.875a2.97 2.97 0 002.105.87 2.97 2.97 0 002.104-.87l20.833-20.781a2.964 2.964 0 000-4.2z" fill="#A02140"></path></svg>',
                        content:'<p>Сервис Sendsay внесен в реестр операторов персональных данных Роскомнадзора и полностью <strong>соответствует требованиям 152-ФЗ «О персональных данных».</strong></p>',
                    },
                    {
                        id:2,
                        title:'Автоматизация рассылок',
                        icon:'<svg width="140" height="140" viewBox="0 0 140 140" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M62.89 52.5a2.735 2.735 0 000 5.469 2.735 2.735 0 000-5.469zM62.89 85.313a2.735 2.735 0 000 5.468 2.735 2.735 0 000-5.469zM62.89 118.125a2.735 2.735 0 10.002 5.47 2.735 2.735 0 00-.001-5.47zM115.391 47.031c-4.524 0-8.203 3.68-8.203 8.203 0 4.524 3.679 8.203 8.203 8.203 4.523 0 8.203-3.68 8.203-8.203s-3.68-8.203-8.203-8.203zm0 10.938a2.738 2.738 0 01-2.735-2.735 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.735zM30.078 52.5H19.141a2.735 2.735 0 000 5.469h10.937a2.735 2.735 0 000-5.469zM51.953 52.5H41.016a2.735 2.735 0 000 5.469h10.937a2.735 2.735 0 000-5.469zM115.391 79.844c-4.524 0-8.203 3.68-8.203 8.203s3.679 8.203 8.203 8.203c4.523 0 8.203-3.68 8.203-8.203s-3.68-8.203-8.203-8.203zm0 10.937a2.738 2.738 0 01-2.735-2.734 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.734zM30.078 85.313H19.141a2.735 2.735 0 000 5.468h10.937a2.734 2.734 0 000-5.469zM51.953 85.313H41.016a2.735 2.735 0 000 5.468h10.937a2.734 2.734 0 000-5.469zM115.391 112.656c-4.524 0-8.203 3.68-8.203 8.203 0 4.524 3.679 8.203 8.203 8.203 4.523 0 8.203-3.679 8.203-8.203 0-4.523-3.68-8.203-8.203-8.203zm0 10.938a2.738 2.738 0 01-2.735-2.735 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.735z" fill="#A02140"></path><path d="M140 42.7c0-2.866-.652-5.796-1.99-8.5L124.87 4.648A8.16 8.16 0 00117.48 0H22.52a8.16 8.16 0 00-7.39 4.648L1.99 34.2A19.22 19.22 0 000 42.7v23.472c0 2.1.794 4.016 2.096 5.469A8.168 8.168 0 000 77.109v21.875c0 2.1.794 4.017 2.096 5.469A8.169 8.169 0 000 109.922v21.875C0 136.32 3.68 140 8.203 140h123.594c4.523 0 8.203-3.68 8.203-8.203v-21.875a8.17 8.17 0 00-2.096-5.469A8.167 8.167 0 00140 98.984V77.11c0-2.1-.794-4.016-2.096-5.468A8.169 8.169 0 00140 66.17V42.7zM20.097 6.936a2.72 2.72 0 012.423-1.467h94.96c1.025 0 1.948.56 2.423 1.467l12.964 29.158H7.133L20.097 6.936zm114.434 124.861a2.736 2.736 0 01-2.734 2.734H8.203a2.737 2.737 0 01-2.734-2.734v-21.875a2.737 2.737 0 012.734-2.734h123.594a2.736 2.736 0 012.734 2.734v21.875zm0-32.813a2.737 2.737 0 01-2.734 2.735H8.203a2.738 2.738 0 01-2.734-2.735V77.11a2.737 2.737 0 012.734-2.734h123.594a2.737 2.737 0 012.734 2.734v21.875zm0-32.812a2.737 2.737 0 01-2.734 2.734H8.203a2.737 2.737 0 01-2.734-2.734V42.7c0-.38.021-.76.053-1.138h128.956c.032.378.053.758.053 1.138v23.472z" fill="#A02140"></path><path d="M30.078 118.125H19.141a2.734 2.734 0 100 5.469h10.937a2.735 2.735 0 000-5.469zM51.953 118.125H41.016a2.734 2.734 0 100 5.469h10.937a2.735 2.735 0 000-5.469z" fill="#A02140"></path></svg>',
                        content:'<p>Наши <strong>собственные серверы расположены в нескольких надёжных дата-центрах</strong> на территории России</p>',
                    },
                    {
                        id:3,
                        title:'Автоматизация рассылок',
                        icon:'<svg width="125" height="152" viewBox="0 0 125 152" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M62.5 45.125a2.974 2.974 0 00-2.973 3.114l-.19.018.074-.003.116-.006a2.978 2.978 0 005.946-.006c.003-.047.003-.098.003-.148a2.974 2.974 0 00-2.976-2.969z" fill="#A02140"></path><path d="M124.009 18.08a2.983 2.983 0 00-2.313-.74 78.59 78.59 0 01-8.601.473c-18.711 0-37.493-8.903-48.83-17.234a2.982 2.982 0 00-3.53 0C49.398 8.91 30.616 17.813 11.905 17.813a78.75 78.75 0 01-8.601-.472 2.983 2.983 0 00-2.313.739A2.966 2.966 0 000 20.292v51.774c0 36.986 27.2 68.958 61.604 79.796a2.98 2.98 0 001.792 0C97.77 141.033 125 109.084 125 72.066V20.291c0-.844-.36-1.648-.991-2.211zm-4.961 53.986c0 32.702-23.205 62.939-56.548 73.847-33.343-10.908-56.548-41.145-56.548-73.847V23.54c1.98.14 3.972.21 5.953.21 19.202 0 38.364-8.625 50.595-17.13 12.23 8.505 31.393 17.13 50.595 17.13 1.981 0 3.973-.07 5.953-.21v48.526z" fill="#A02140"></path><path d="M110.24 29.628c-14.753-.596-31.59-6.034-46.19-14.917a2.983 2.983 0 00-3.1 0c-14.6 8.883-31.437 14.32-46.19 14.917a2.972 2.972 0 00-2.855 2.966v39.473c0 28.688 20.377 56.316 49.554 67.188a2.983 2.983 0 002.082 0c29.177-10.872 49.554-38.501 49.554-67.189V32.594a2.972 2.972 0 00-2.855-2.966zm-3.097 42.438c0 25.922-18.302 50.947-44.643 61.229-26.341-10.282-44.643-35.307-44.643-61.229V35.395c19.16-1.447 35.57-9.37 44.643-14.69 9.074 5.32 25.484 13.243 44.643 14.69v36.671z" fill="#A02140"></path><path d="M75.34 47.734a2.979 2.979 0 00-3.909 1.56 2.966 2.966 0 001.563 3.898c9.897 4.23 16.292 13.883 16.292 24.59 0 15.181-12.517 28.499-26.786 28.499-14.006 0-27.005-13.068-26.78-29.058.327-10.829 6.414-19.81 16.286-24.032a2.966 2.966 0 001.563-3.898 2.979 2.979 0 00-3.908-1.559C37.775 52.815 30.153 64.056 29.769 77.07v.047c-.265 18.302 14.319 35.102 32.731 35.102 18.328 0 32.738-16.657 32.738-34.438 0-13.085-7.81-24.879-19.899-30.047z" fill="#A02140"></path><path d="M79.485 66.776a2.982 2.982 0 00-4.209 0L56.548 85.458l-9.8-9.776a2.982 2.982 0 00-4.21 0 2.964 2.964 0 000 4.199l11.905 11.875a2.97 2.97 0 002.105.87 2.97 2.97 0 002.104-.87l20.833-20.781a2.964 2.964 0 000-4.2z" fill="#A02140"></path></svg>',
                        content:'<p>Сервис Sendsay внесен в реестр операторов персональных данных Роскомнадзора и полностью <strong>соответствует требованиям 152-ФЗ «О персональных данных».</strong></p>',
                    },
                    {
                        id:4,
                        title:'Автоматизация рассылок',
                        icon:'<svg width="140" height="140" viewBox="0 0 140 140" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M62.89 52.5a2.735 2.735 0 000 5.469 2.735 2.735 0 000-5.469zM62.89 85.313a2.735 2.735 0 000 5.468 2.735 2.735 0 000-5.469zM62.89 118.125a2.735 2.735 0 10.002 5.47 2.735 2.735 0 00-.001-5.47zM115.391 47.031c-4.524 0-8.203 3.68-8.203 8.203 0 4.524 3.679 8.203 8.203 8.203 4.523 0 8.203-3.68 8.203-8.203s-3.68-8.203-8.203-8.203zm0 10.938a2.738 2.738 0 01-2.735-2.735 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.735zM30.078 52.5H19.141a2.735 2.735 0 000 5.469h10.937a2.735 2.735 0 000-5.469zM51.953 52.5H41.016a2.735 2.735 0 000 5.469h10.937a2.735 2.735 0 000-5.469zM115.391 79.844c-4.524 0-8.203 3.68-8.203 8.203s3.679 8.203 8.203 8.203c4.523 0 8.203-3.68 8.203-8.203s-3.68-8.203-8.203-8.203zm0 10.937a2.738 2.738 0 01-2.735-2.734 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.734zM30.078 85.313H19.141a2.735 2.735 0 000 5.468h10.937a2.734 2.734 0 000-5.469zM51.953 85.313H41.016a2.735 2.735 0 000 5.468h10.937a2.734 2.734 0 000-5.469zM115.391 112.656c-4.524 0-8.203 3.68-8.203 8.203 0 4.524 3.679 8.203 8.203 8.203 4.523 0 8.203-3.679 8.203-8.203 0-4.523-3.68-8.203-8.203-8.203zm0 10.938a2.738 2.738 0 01-2.735-2.735 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.735z" fill="#A02140"></path><path d="M140 42.7c0-2.866-.652-5.796-1.99-8.5L124.87 4.648A8.16 8.16 0 00117.48 0H22.52a8.16 8.16 0 00-7.39 4.648L1.99 34.2A19.22 19.22 0 000 42.7v23.472c0 2.1.794 4.016 2.096 5.469A8.168 8.168 0 000 77.109v21.875c0 2.1.794 4.017 2.096 5.469A8.169 8.169 0 000 109.922v21.875C0 136.32 3.68 140 8.203 140h123.594c4.523 0 8.203-3.68 8.203-8.203v-21.875a8.17 8.17 0 00-2.096-5.469A8.167 8.167 0 00140 98.984V77.11c0-2.1-.794-4.016-2.096-5.468A8.169 8.169 0 00140 66.17V42.7zM20.097 6.936a2.72 2.72 0 012.423-1.467h94.96c1.025 0 1.948.56 2.423 1.467l12.964 29.158H7.133L20.097 6.936zm114.434 124.861a2.736 2.736 0 01-2.734 2.734H8.203a2.737 2.737 0 01-2.734-2.734v-21.875a2.737 2.737 0 012.734-2.734h123.594a2.736 2.736 0 012.734 2.734v21.875zm0-32.813a2.737 2.737 0 01-2.734 2.735H8.203a2.738 2.738 0 01-2.734-2.735V77.11a2.737 2.737 0 012.734-2.734h123.594a2.737 2.737 0 012.734 2.734v21.875zm0-32.812a2.737 2.737 0 01-2.734 2.734H8.203a2.737 2.737 0 01-2.734-2.734V42.7c0-.38.021-.76.053-1.138h128.956c.032.378.053.758.053 1.138v23.472z" fill="#A02140"></path><path d="M30.078 118.125H19.141a2.734 2.734 0 100 5.469h10.937a2.735 2.735 0 000-5.469zM51.953 118.125H41.016a2.734 2.734 0 100 5.469h10.937a2.735 2.735 0 000-5.469z" fill="#A02140"></path></svg>',
                        content:'<p>Наши <strong>собственные серверы расположены в нескольких надёжных дата-центрах</strong> на территории России</p>',
                    },
                    {
                        id:5,
                        title:'Автоматизация рассылок',
                        icon:'<svg width="140" height="140" viewBox="0 0 140 140" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M62.89 52.5a2.735 2.735 0 000 5.469 2.735 2.735 0 000-5.469zM62.89 85.313a2.735 2.735 0 000 5.468 2.735 2.735 0 000-5.469zM62.89 118.125a2.735 2.735 0 10.002 5.47 2.735 2.735 0 00-.001-5.47zM115.391 47.031c-4.524 0-8.203 3.68-8.203 8.203 0 4.524 3.679 8.203 8.203 8.203 4.523 0 8.203-3.68 8.203-8.203s-3.68-8.203-8.203-8.203zm0 10.938a2.738 2.738 0 01-2.735-2.735 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.735zM30.078 52.5H19.141a2.735 2.735 0 000 5.469h10.937a2.735 2.735 0 000-5.469zM51.953 52.5H41.016a2.735 2.735 0 000 5.469h10.937a2.735 2.735 0 000-5.469zM115.391 79.844c-4.524 0-8.203 3.68-8.203 8.203s3.679 8.203 8.203 8.203c4.523 0 8.203-3.68 8.203-8.203s-3.68-8.203-8.203-8.203zm0 10.937a2.738 2.738 0 01-2.735-2.734 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.734zM30.078 85.313H19.141a2.735 2.735 0 000 5.468h10.937a2.734 2.734 0 000-5.469zM51.953 85.313H41.016a2.735 2.735 0 000 5.468h10.937a2.734 2.734 0 000-5.469zM115.391 112.656c-4.524 0-8.203 3.68-8.203 8.203 0 4.524 3.679 8.203 8.203 8.203 4.523 0 8.203-3.679 8.203-8.203 0-4.523-3.68-8.203-8.203-8.203zm0 10.938a2.738 2.738 0 01-2.735-2.735 2.738 2.738 0 012.735-2.734 2.738 2.738 0 012.734 2.734 2.738 2.738 0 01-2.734 2.735z" fill="#A02140"></path><path d="M140 42.7c0-2.866-.652-5.796-1.99-8.5L124.87 4.648A8.16 8.16 0 00117.48 0H22.52a8.16 8.16 0 00-7.39 4.648L1.99 34.2A19.22 19.22 0 000 42.7v23.472c0 2.1.794 4.016 2.096 5.469A8.168 8.168 0 000 77.109v21.875c0 2.1.794 4.017 2.096 5.469A8.169 8.169 0 000 109.922v21.875C0 136.32 3.68 140 8.203 140h123.594c4.523 0 8.203-3.68 8.203-8.203v-21.875a8.17 8.17 0 00-2.096-5.469A8.167 8.167 0 00140 98.984V77.11c0-2.1-.794-4.016-2.096-5.468A8.169 8.169 0 00140 66.17V42.7zM20.097 6.936a2.72 2.72 0 012.423-1.467h94.96c1.025 0 1.948.56 2.423 1.467l12.964 29.158H7.133L20.097 6.936zm114.434 124.861a2.736 2.736 0 01-2.734 2.734H8.203a2.737 2.737 0 01-2.734-2.734v-21.875a2.737 2.737 0 012.734-2.734h123.594a2.736 2.736 0 012.734 2.734v21.875zm0-32.813a2.737 2.737 0 01-2.734 2.735H8.203a2.738 2.738 0 01-2.734-2.735V77.11a2.737 2.737 0 012.734-2.734h123.594a2.737 2.737 0 012.734 2.734v21.875zm0-32.812a2.737 2.737 0 01-2.734 2.734H8.203a2.737 2.737 0 01-2.734-2.734V42.7c0-.38.021-.76.053-1.138h128.956c.032.378.053.758.053 1.138v23.472z" fill="#A02140"></path><path d="M30.078 118.125H19.141a2.734 2.734 0 100 5.469h10.937a2.735 2.735 0 000-5.469zM51.953 118.125H41.016a2.734 2.734 0 100 5.469h10.937a2.735 2.735 0 000-5.469z" fill="#A02140"></path></svg>',
                        content:'<p>Наши <strong>собственные серверы расположены в нескольких надёжных дата-центрах</strong> на территории России</p>',
                    },
              

                ]
            }
        },
        clientListByCategoryPseudoApi(){
            return {
                itemList:[
                    {
                        name:'Все',
                        content:`<div class="h1">Все</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[
                            {url:'/upload/img/preview-client-01.png'},
                            {url:'/upload/img/preview-client-02.png'},
                            {url:'/upload/img/preview-client-03.png'},
                            {url:'/upload/img/preview-client-04.png'},
                            {url:'/upload/img/preview-client-05.png'},
                            {url:'/upload/img/preview-client-06.png'},
                            {url:'/upload/img/preview-client-01.png'},
                            {url:'/upload/img/preview-client-02.png'},
                            {url:'/upload/img/preview-client-03.png'},
                            {url:'/upload/img/preview-client-04.png'},
                            {url:'/upload/img/preview-client-05.png'},
                            {url:'/upload/img/preview-client-06.png'},
                            {url:'/upload/img/preview-client-06.png'},
                            /* {url:'/upload/img/preview-client-06.png'},
                             {url:'/upload/img/preview-client-01.png'},
                             {url:'/upload/img/preview-client-02.png'},
                             {url:'/upload/img/preview-client-03.png'},
                             {url:'/upload/img/preview-client-04.png'},
                             {url:'/upload/img/preview-client-05.png'},
                             {url:'/upload/img/preview-client-06.png'},*/
                        ]
                    },
                    {
                        name:'Недвижимость',
                        content:`<div class="h1">Недвижимость</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[
                            {url:'/upload/img/preview-client-01.png'},
                            {url:'/upload/img/preview-client-02.png'},
                            {url:'/upload/img/preview-client-03.png'},


                        ]
                    },
                    {
                        name:'Инфобизнес',
                        content:`<div class="h1">Инфобизнес</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[
                            {url:'/upload/img/preview-client-01.png'},
                            {url:'/upload/img/preview-client-02.png'},
                            {url:'/upload/img/preview-client-03.png'},


                        ]
                    },
                    {
                        name:'СМИ',
                        content:`<div class="h1">Инфобизнес</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[
                            {url:'/upload/img/preview-client-01.png'},
                        ]
                    },
                    {
                        name:'Выставки',
                        content:`<div class="h1">Выставки</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[
                            {url:'/upload/img/preview-client-01.png'},
                        ]
                    },
                    {
                        name:'Финансы и страхования',
                        content:`<div class="h1">Финансы и страхования</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[

                        ]
                    },
                    {
                        name:'Офлайн ритейл',
                        content:`<div class="h1">Офлайн ритейл</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[

                        ]
                    },
                    {
                        name:'E-com',
                        content:`<div class="h1">Офлайн ритейл</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,
                        logoList:[

                        ]
                    },
                    {
                        name:'Рестораны',
                        content:`<div class="h1">Офлайн ритейл</div><div class="h4">более 100 клиентов</div><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>`,

                    },
                ]
            }
        },
        clientListWithReviewsPseudoApi(){
            return [
                {
                    title:'Втб',
                    label:'Крупнейший частный банк в России',
                    content:'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url:'/upload/img/preview-client-03.png'
                },
                {
                    title:'КИВИ',
                    label:'Крупнейший частный банк в России',
                    content:'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url:'/upload/img/preview-client-01.png'
                },
                {
                    title: 'Альфа-банк',
                    label: 'Крупнейший частный банк в России',
                    content: 'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url: '/upload/img/preview-client-02.png'
                },
                {
                    title: 'Росгос страх',
                    label: 'Крупнейший частный банк в России',
                    content: 'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url: '/upload/img/preview-client-04.png'
                },
                {
                    title: 'Ренесанс',
                    label: 'Крупнейший частный банк в России',
                    content: 'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url: '/upload/img/preview-client-05.png'
                },
                {
                    title: 'Пятерочка',
                    label: 'Крупнейший частный банк в России',
                    content: 'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url: '/upload/img/preview-client-06.png'
                },
                {
                    title:'Втб',
                    label:'Крупнейший частный банк в России',
                    content:'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url:'/upload/img/preview-client-03.png'
                },
                {
                    title:'КИВИ',
                    label:'Крупнейший частный банк в России',
                    content:'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url:'/upload/img/preview-client-01.png'
                },
                {
                    title: 'Росгос страх',
                    label: 'Крупнейший частный банк в России',
                    content: 'Сотрудничаем с 2006 года, отметим расширенные функциональные возможности платформы и готовы рекомендовать Sendsay как надежную платформу для построения эффективного email-маркетинга.',
                    url: '/upload/img/preview-client-04.png'
                },
            ]
        },
        vChessPseudoApi() {
            return {
                title:'Тестовый заголовок',
                content:'<p>Тестовый контент</p>',
                features:[
                    {
                        id:1,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-05.png',
                        content:'<p>Тестовый контент</p>',
                        button_text:'nuxt-link',
                        button_url:'/'
                    },
                    {
                        id:2,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-03.png',
                        content:'<p>Тестовый контент</p>',
                        button_text:'href link',
                        button_url:'http://ya.ru'
                    },
                    {
                        id:3,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-06.png',
                        content:'<p>Тестовый контент</p>',
                        button_text:'Посмотреть все интеграции',
                        button_url:'http://ya.ru'
                    },
                    {
                        id:4,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-07.png',
                        content:'<p>Тестовый контент</p>'
                    },

                ]
            }
        },
        vRegBanner() {
            return {
                title:'Тестовый заголовок',
                content:'<p>Тестовый контент</p>',
                features:[
                    {
                        id:1,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-05.png',
                        content:'<p>Тестовый контент</p>',
                        button_text:'nuxt-link',
                        button_url:'/'
                    },
                    {
                        id:2,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-03.png',
                        content:'<p>Тестовый контент</p>',
                        button_text:'href link',
                        button_url:'http://ya.ru'
                    },
                    {
                        id:3,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-06.png',
                        content:'<p>Тестовый контент</p>',
                        button_text:'Посмотреть все интеграции',
                        button_url:'http://ya.ru'
                    },
                    {
                        id:4,
                        title:'Автоматизация рассылок',
                        image:'/upload/img/banner-07.png',
                        content:'<p>Тестовый контент</p>'
                    },

                ]
            }
        }
    }
}
