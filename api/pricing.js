export default {
  plans () { return `pricing/` },
  plan (slug) { return `pricing/${slug}/` }
}
