export default {
  change_password () { return `authorization/change_password/` },
  login () { return `authorization/login/` },
  logout () { return `authorization/logout/` },
  register () { return `authorization/register/` },
  restore () { return `authorization/restore/` }
}
