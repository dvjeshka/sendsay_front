export default {
  opportunities () { return `services/opportunities/` },
  opportunity (slug) { return `services/opportunities/${slug}/` },
  services () { return `services/services/` },
  service (slug) { return `services/services/${slug}/` },
  solutions () { return `services/solutions/` },
  solution (slug) { return `services/solutions/${slug}/` }
}
