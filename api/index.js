import about from './about'
import authorization from './authorization'
import blog from './blog'
import contacts from './contacts'
import forms from './forms'
import my from './my'
import pages from './pages'
import pricing from './pricing'
import services from './services'
import vars from './vars'

export default {
  about,
  authorization,
  blog,
  contacts,
  forms,
  my,
  pages,
  pricing,
  services,
  vars
}
