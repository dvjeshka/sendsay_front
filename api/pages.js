export default {
  contacts () { return `pages/contacts/` },
  flatPage (slug) { return `pages/flatpages/${slug}/` },
  homepage () { return `pages/homepage/` }
}
