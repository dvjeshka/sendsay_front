export const intcomma = (value, removeZeros) => {
  if (isNaN(value)) {
    return '' + value
  }

  if (typeof (removeZeros) !== 'undefined' && removeZeros) {
    value = parseFloat(value)
    if (value % 1 === 0) {
      value = parseInt(value)
    }
  }

  value += ''
  let x = value.split('.')
  let x1 = x[0]
  let x2 = x.length > 1 ? '.' + x[1] : ''
  let rgx = /(\d+)(\d{3})/

  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1 $2')
  }
  return x1 + x2
}

export const phone = value => {
  value = '' + value
  if (value.indexOf('tel:') === 0) {
    value = value.split('tel:')[1]
  }
  return value.replace(/[^\d]/g, '')
}

export const phoneUrl = value => 'tel:+' + phone(value)

export default {
  intcomma,
  phone,
  phoneUrl
}
