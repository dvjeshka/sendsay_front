//import { INITIAL_VIEWPORTS} from '@storybook/addon-viewport';
const config = require('./config')
const path = require('path')
export default {
    server: {
        port: 8001, // default: 3000
        host: '0.0.0.0' // default: localhost
    },
    // Target (https://go.nuxtjs.dev/config-target)
    target: 'static',

    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'sendsay_front',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            { hid: 'description', name: 'description', content: '' },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/css2?family=Open+Sans&display=swap'
            }
            ],
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [
        // '~/plugins/resp.client.js',
        '~/plugins/main.js',
        '~/plugins/vue-js-modal.js',
        
        //'~/plugins/nuxt-swiper-plugin.client.js',
        '~/plugins/vue-element-resize-decorator.client.js'
    ],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        // '@nuxtjs/eslint-module',
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        '@nuxtjs/proxy',
        '@nuxtjs/axios',
        [
            'svg-to-vue-component/nuxt',
            {
                // ...
            },
        ],
        '@nuxtjs/style-resources',
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
    ],

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {
        baseURL: config.baseURL,
        browserBaseURL: config.baseURL,
        https: true,
        proxy: true,
        proxyHeaders: true
    },
    proxy: {
       /* '/api': {
            target: config.baseURL,
            pathRewrite: { '^/api': '' }
        },*/
        '/media': {
            target: config.mediaURL,
            pathRewrite: { '^/media': '' }
        }
    },

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {
        extend(config, {loaders}) {

          /*  config.module.rules.push(
                {
                    test: /\.scss$/,
                    use: [
                        'vue-style-loader',
                        'css-loader',
                        {
                            loader: 'sass-loader'
                        },
                        {
                            loader: '@epegzz/sass-vars-loader',
                            options: {
                                // You can specify vars here
                                
                                files: [
                               
                                    // You can include JavaScript files
                                    path.resolve(__dirname, 'config/sassVars.js'),
                                ],
                            },
                        },
                    ]
                },
                {
                    test: /\.sass$/,
                    use: [
                        'vue-style-loader',
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                indentedSyntax: true
                            }
                        }
                    ]
                }
            )*/
            /*      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'));

            svgRule.test = /\.(png|jpe?g|gif|webp)$/;

            config.module.rules.push({
                test: /\.svg$/,
                use: [
                    'babel-loader',
                    'vue-svg-loader',
                ],

            }); */
           /* const scssRule = config.module.rules.find(rule => rule.test.test('.scss'));
            console.log(scssRule);*/

           
            /** Define the sass-vars-loader with options */
            const sassVarsLoader = {
                loader: '@epegzz/sass-vars-loader',
                options: {
                    files: [
                        path.resolve(__dirname, 'config/sassVars.js'),
                        //path.resolve(__dirname, 'config/viewports.js'),
                    ]
                }
            }
            /** Run .scss files through sass-vars-loader before anything else */
            const scssLoader = config.module.rules.find((loader) => loader.test.test('.scss'));
          
            scssLoader.oneOf.forEach(item=>{
                item.use.push(sassVarsLoader);
            })
            //scssLoader.use.splice(5, 0, sassVarsLoader)
            
            /** Run .vue files' scss through sass-vars-loader before anything else */
          /*  const vueLoader = config.module.rules.find((loader) => loader.test.test('.vue'));
            console.log(vueLoader);
            vueLoader.oneOf.forEach(item=>{
                item.use.push(sassVarsLoader);
            })*/
            //vueLoader.options.loaders.scss.splice(5, 0, sassVarsLoader)
       /*     scssRule
                .oneOf('vue')
                .use('sass-vars-loader')
                .loader('@epegzz/sass-vars-loader')
                .before('sass-loader')

            config.module
                .rule('scss')
                .oneOf('normal')
                .use('sass-vars-loader')
                .loader('@epegzz/sass-vars-loader')
                .before('sass-loader')*/
            
        },
    },
    styleResources: {
        // your settings here
        scss: ['@/assets/scss/config.scss'], // alternative: scss
    },
    /*storybook: {
        port: 3500,
        stories: ['~/stories/!**!/!*.stories.js'],
        addons: [
          
            '@storybook/addon-viewport',
            '@storybook/addon-controls',
            '@storybook/addon-docs'
        ],
        parameters: {
            viewport: {
                viewports: {
                    ...INITIAL_VIEWPORTS,
                    desctopLg: {
                        name: 'desctopLg',
                        styles: {
                            width: '2000px',
                            height: '963px',
                        },
                    },
                },
            },
        }
    },*/
}
